package com.react.app.controller;

import com.react.app.service.FileManager;
import com.react.app.service.MessageManager;
import com.react.app.service.UserManager;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*")
public class AppController {

    private static final Logger logger = LoggerFactory.getLogger(AppController.class);

    private final MessageManager messageManager;
    private final UserManager userManager;

    public AppController(MessageManager messageManager, UserManager userManager) {
        this.messageManager = messageManager;
        this.userManager = userManager;
    }

    @GetMapping("/messages")
    private String messagesGetRequestHandler() {
        logger.info("messagesGetRequestHandler");
        return messageManager.getAllMessages();
    }

    @DeleteMapping("/messages/{id}")
    private void messageDeleteRequestHandler(@PathVariable String id) {
        logger.info("messageDeleteRequestHandler");
        messageManager.deleteMessage(Integer.valueOf(id));
    }

    @PutMapping("/messages")
    private void messagePutRequestHandler(@RequestBody String messageJson) {
        logger.info("messagePutRequestHandler");
        messageManager.updateMessage(new JSONObject(messageJson));
    }

    @PostMapping("/messages")
    private String messagePostRequestHandler(@RequestBody String messageDataJson) {
        logger.info("messagePostRequestHandler");
        return messageManager.addMessage(new JSONObject(messageDataJson)).toString();
    }

    @GetMapping("/users")
    private String usersGetRequestHandler() {
        logger.info("usersGetRequestHandler");
        return userManager.getAllUsers();
    }

    @DeleteMapping("/users/{id}")
    private void userDeleteRequestHandler(@PathVariable String id) {
        logger.info("userDeleteRequestHandler");
        userManager.deleteUser(Integer.valueOf(id));
    }

    @PutMapping("/users")
    private void userPutRequestHandler(@RequestBody String userJson) {
        logger.info("userPutRequestHandler");
        userManager.updateUser(new JSONObject(userJson));
    }

    @PostMapping("/users")
    private String userPostRequestHandler(@RequestBody String userDataJson) {
        logger.info("userPostRequestHandler");
        return userManager.addUser(new JSONObject(userDataJson)).toString();
    }

    @GetMapping("/users/count")
    private Integer getUsersCountRequestHanlder() {
        logger.info("getUsersCountRequestHanlder");
        return userManager.getUsersCount();
    }

    @PostMapping("/auth")
    private String authGetRequestHandler(@RequestBody String authData) {
        logger.info("authGetRequestHandler");
        return userManager.authentication(new JSONObject(authData));
    }

}
