package com.react.app.service;

import com.react.app.controller.AppController;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class MessageManager {

    private static final Logger logger = LoggerFactory.getLogger(MessageManager.class);

    @Autowired
    private FileManager fileManager;

    public JSONObject addMessage(JSONObject data) {
        logger.info("addMessage");
        try {
            JSONArray messages = new JSONArray(fileManager.readAllFromFile("./data/messages"));
            List < JSONObject > messageJsonList = IntStream
                    .range(0, messages.length())
                    .mapToObj(i -> messages.getJSONObject(i))
                    .sorted(Comparator.comparingInt(a -> a.getInt("id")))
                    .collect(Collectors.toList());
            data.put("id", messageJsonList.get(messageJsonList.size() - 1).getInt("id") + 1);
            messageJsonList.add(data);
            fileManager.saveToFile("./data/messages", new JSONArray(messageJsonList).toString());
            return data;
        } catch (IOException e) {
            logger.error(e.getMessage());
            return new JSONObject("{}");
        }
    }

    public void updateMessage(JSONObject message) {
        logger.info("updateMessage");
        try {
            JSONArray messages = new JSONArray(fileManager.readAllFromFile("./data/messages"));
            fileManager.saveToFile("./data/messages", new JSONArray(IntStream
                    .range(0, messages.length())
                    .mapToObj(i -> messages.getJSONObject(i))
                    .map(i -> i.getInt("id") == message.getInt("id") ? message : i)
                    .collect(Collectors.toList())).toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public void deleteMessage(Integer id) {
        logger.info("deleteMessage");
        try {
            JSONArray messages = new JSONArray(fileManager.readAllFromFile("./data/messages"));
            fileManager.saveToFile("./data/messages", new JSONArray(IntStream
                        .range(0, messages.length())
                        .mapToObj(i -> messages.getJSONObject(i))
                        .filter(i -> {
                            return i.getInt("id") != id;
                        })
                        .collect(Collectors.toList())
                    ).toString()
            );
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String getAllMessages() {
        logger.info("getAllMessages");
        try {
            return fileManager.readAllFromFile("./data/messages");
        } catch (IOException e) {
            logger.error(e.getMessage());
            return "[]";
        }
    }

}
