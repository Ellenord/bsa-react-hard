package com.react.app.service;

import org.apache.catalina.User;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class UserManager {

    private static final Logger logger = LoggerFactory.getLogger(UserManager.class);

    @Autowired
    private FileManager fileManager;

    public JSONObject addUser(JSONObject data) {
        logger.info("addUser");
        try {
            JSONArray users = new JSONArray(fileManager.readAllFromFile("./data/users"));
            List < JSONObject > userJsonList = IntStream
                    .range(0, users.length())
                    .mapToObj(i -> users.getJSONObject(i))
                    .sorted(Comparator.comparingInt(a -> a.getInt("id")))
                    .collect(Collectors.toList());
            data.put("id", userJsonList.get(userJsonList.size() - 1).getInt("id") + 1);
            userJsonList.add(data);
            fileManager.saveToFile("./data/users", new JSONArray(userJsonList).toString());
            return data;
        } catch (IOException e) {
            logger.error(e.getMessage());
            return new JSONObject("{}");
        }
    }

    public void updateUser(JSONObject user) {
        logger.info("updateUser");
        try {
            JSONArray users = new JSONArray(fileManager.readAllFromFile("./data/users"));
            fileManager.saveToFile("./data/users", new JSONArray(IntStream
                    .range(0, users.length())
                    .mapToObj(i -> users.getJSONObject(i))
                    .map(i -> i.getInt("id") == user.getInt("id") ? user : i)
                    .collect(Collectors.toList())).toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public void deleteUser(Integer id) {
        logger.info("deleteUser");
        try {
            JSONArray users = new JSONArray(fileManager.readAllFromFile("./data/users"));
            fileManager.saveToFile("./data/users", new JSONArray(IntStream
                            .range(0, users.length())
                            .mapToObj(i -> users.getJSONObject(i))
                            .filter(i -> i.getInt("id") != id)
                            .collect(Collectors.toList())
                    ).toString()
            );
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String getAllUsers() {
        logger.info("getAllUsers");
        try {
            return fileManager.readAllFromFile("./data/users");
        } catch (IOException e) {
            logger.error(e.getMessage());
            return "[]";
        }
    }

    public String authentication(JSONObject authData) {
        String login = authData.getString("login");
        String password = authData.getString("password");
        if (login.equals("admin")) {
            return !password.equals("admin") ? null : "{"
                    + "\"id\": -1,"
                    + "\"name\": \"admin\","
                    + "\"avatar\": \"\""
                    + "}";
        }
        JSONArray users = new JSONArray(getAllUsers());
        List < JSONObject > userJsonList = IntStream
                .range(0, users.length())
                .mapToObj(i -> users.getJSONObject(i))
                .filter(i -> i.getString("login").equals(login))
                .collect(Collectors.toList());
        return userJsonList.isEmpty() || !userJsonList.get(0).getString("password").equals(password) ? null : userJsonList.get(0).toString();
    }

    public Integer getUsersCount() {
        try {
            return new JSONArray(fileManager.readAllFromFile("./data/users")).length();
        } catch (IOException e) {
            logger.error(e.getMessage());
            return 0;
        }
    }

}
