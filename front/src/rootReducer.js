import { combineReducers } from 'redux';
import messageListReducer from './MessageList/reducer';
import userListReducer from './UserList/reducer';
import messageEditFormReducer from './EditForm/reducer';
import authorizationReducer from './Login/reducer';

const rootReducer = combineReducers({
  messageListReducer,
  userListReducer,
  authorizationReducer,
  messageEditFormReducer
});

export default rootReducer;
