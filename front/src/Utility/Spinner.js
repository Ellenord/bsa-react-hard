import React from 'react';
import './Spinner.css';

class Spinner extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div class="background">
        <div class="loader">
          <div class="inner one"></div>
          <div class="inner two"></div>
          <div class="inner three"></div>
        </div>
      </div>
    );
  }

}

export default Spinner;
