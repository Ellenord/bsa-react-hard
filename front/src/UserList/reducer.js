import {
  INIT_USERS_SUCCESS, ADD_USER_SUCCESS,
  UPDATE_USER_SUCCESS, DELETE_USER_SUCCESS,
  GET_USERS_COUNT, SET_USERS_COUNT
} from './actionTypes.js';

const initialState = [];

export default function(state = initialState, action) {
  switch (action.type) {
    case INIT_USERS_SUCCESS: {
      let { users } = action.payload;
      return users;
    }
    case ADD_USER_SUCCESS: {
      const { message } = action.payload;
      return [...state, message];
    }
    case UPDATE_USER_SUCCESS: {
      const { message } = action.payload;
      return state.map(i => { return i.id == message.id ? message : i; });
    }
    case DELETE_USER_SUCCESS: {
      const { id } = action.payload;
      return state.filter(user => user.id != id);
    }
    case SET_USERS_COUNT: {
      const { count } = action.payload;
      return [ count ];
    }
    default:
      return state;
  }
};
