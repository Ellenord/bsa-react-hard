import { all, call, put, takeEvery } from 'redux-saga/effects';
import {
  INIT_USERS, GET_USERS_COUNT,
  ADD_USER, UPDATE_USER, DELETE_USER
} from './actionTypes';
import {
  initUsersSuccess, setUsersCount,
  addUserSuccess, updateUserSuccess, deleteUserSuccess
} from './actions';
import makeHttpRequest from '../HttpService';

const targetUrl = 'http://localhost:8080/users';

function* initUsers(action) {
  try {
    const users = JSON.parse(yield makeHttpRequest(targetUrl, 'GET'));
    yield put(initUsersSuccess(users));
  } catch (e) {}
}

function* getUsersCount(action) {
  try {
    const usersCount = yield makeHttpRequest(targetUrl + '/count', 'GET');
    yield put(setUsersCount(usersCount));
  } catch (e) {}
}

function* addUser(action) {
  try {
    const { data } = action.payload;
    const user = JSON.parse(yield makeHttpRequest(targetUrl, 'POST', data));
    yield put(addUserSuccess(user));
  } catch (e) {}
}

function* updateUser(action) {
  try {
    const { message } = action.payload;
    yield makeHttpRequest(targetUrl, 'PUT', message);
    yield put(updateUserSuccess(message));
  } catch (e) {}
}

function* deleteUser(action) {
  try {
    const { id } = action.payload;
    yield makeHttpRequest(targetUrl + '/' + id, 'DELETE');
    yield put(deleteUserSuccess(id));
  } catch (e) {}
}

function* watchInitUsers() {
  yield all([
    takeEvery(INIT_USERS, initUsers),
    takeEvery(GET_USERS_COUNT, getUsersCount),
    takeEvery(ADD_USER, addUser),
    takeEvery(UPDATE_USER, updateUser),
    takeEvery(DELETE_USER, deleteUser),
  ]);
}

export default watchInitUsers;
