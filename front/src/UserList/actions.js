import {
  INIT_USERS, INIT_USERS_SUCCESS,
  ADD_USER, ADD_USER_SUCCESS,
  UPDATE_USER, UPDATE_USER_SUCCESS,
  DELETE_USER, DELETE_USER_SUCCESS,
  GET_USERS_COUNT, SET_USERS_COUNT
} from './actionTypes';

export const initUsers = () => ({
  type: INIT_USERS
});
export const initUsersSuccess = (users) => ({
  type: INIT_USERS_SUCCESS,
  payload: {
    users
  }
});

export const addUser = (data) => ({
  type: ADD_USER,
  payload: {
    data
  }
});
export const addUserSuccess = (user) => ({
  type: ADD_USER_SUCCESS,
  payload: {
    user
  }
});

export const updateUser = (user) => ({
  type: UPDATE_USER,
  payload: {
    user
  }
});
export const updateUserSuccess = (user) => ({
  type: UPDATE_USER_SUCCESS,
  payload: {
    user
  }
});

export const deleteUser = (id) => ({
  type: DELETE_USER,
  payload: {
    id
  }
});
export const deleteUserSuccess = (id) => ({
  type: DELETE_USER_SUCCESS,
  payload: {
    id
  }
});

export const getUsersCount = () => ({
  type: GET_USERS_COUNT
});
export const setUsersCount = (count) => ({
  type: SET_USERS_COUNT,
  payload: {
    count
  }
});
