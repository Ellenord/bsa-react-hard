export const INIT_USERS = "INIT_USERS";
export const INIT_USERS_SUCCESS = "INIT_USERS_SUCCESS";

export const ADD_USER = "ADD_USER";
export const ADD_USER_SUCCESS = "ADD_USER_SUCCESS";

export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";

export const DELETE_USER = "DELETE_USER";
export const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";

export const GET_USERS_COUNT = "GET_USERS_COUNT";
export const SET_USERS_COUNT = "SET_USERS_COUNT";
