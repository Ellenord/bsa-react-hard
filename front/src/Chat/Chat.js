import React from 'react';

import { connect } from 'react-redux';

import './Chat.css';

import ChatHeader from './ChatHeader';
import ChatBody from './ChatBody';
import ChatInput from './ChatInput';
import ChatMessage from '../ChatMessage/ChatMessage';

class Chat extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return(
      <div className="Chat">
        <div>
          <div>
            <ChatHeader />
            <ChatBody />
            <ChatInput />
          </div>
        </div>
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
