import React from 'react';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import './ChatBody.css';
import { initMessages } from '../MessageList/actions';

import ChatMessage from '../ChatMessage/ChatMessage';

class ChatBody extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.initMessages();
  }

  static messagesDateComparator(a, b) {
    a = a.date;
    b = b.date;
    if (Number(a.slice(9, 13)) != Number(b.slice(9, 13))) {
      return Number(a.slice(9, 13)) - Number(b.slice(9, 13));
    } else if (Number(a.slice(14, 16)) != Number(b.slice(14, 16))) {
      return Number(a.slice(14, 16)) - Number(b.slice(14, 16));
    } else if (Number(a.slice(17, 19)) != Number(b.slice(17, 19))) {
      return Number(a.slice(17, 19)) - Number(b.slice(17, 19));
    } else if (Number(a.slice(0, 2) != Number(b.slice(0, 2)))) {
      return Number(a.slice(0, 2)) - Number(b.slice(0, 2));
    } else if (Number(a.slice(3, 5)) != Number(b.slice(3, 5))) {
      return Number(a.slice(3, 5)) - Number(b.slice(3, 5));
    } else if (Number(a.slice(6, 8)) != Number(b.slice(6, 8))) {
      return Number(a.slice(6, 8)) - Number(b.slice(6, 8));
    }
  }

  render() {
    return (
      <div id="ChatBody" className="ChatBody">
        {this.props.messages.sort(ChatBody.messagesDateComparator).map((message: Object) => {
            return <ChatMessage
              id={message.id} text={message.text}
              date={message.date} userId={message.userId}
              userAvatar={message.userAvatar} userName={message.userName}
              selfMessage={message}
            />;
          })}
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    messages: state.messageListReducer
  }
};

const mapDispatchToProps = {
  initMessages
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatBody);
