import React from 'react';
import './ChatInput.css';

import { connect } from 'react-redux';

import { addMessage } from '../MessageList/actions';

class ChatInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      shiftPressed: false
    }
    this.handleEnter = this.handleEnter.bind(this);
    this.handleShiftUp = this.handleShiftUp.bind(this);
    this.handleShiftDown = this.handleShiftDown.bind(this);
  }

  static to2char(num) {
    if (num == 0) {
      return '00';
    } else if (num < 10) {
      return '0' + num;
    } else {
      return String(num);
    }
  }

  handleShiftDown(e) {
    if (e.key == "Shift") {
      this.state.shiftPressed = true;
    }
  }

  handleShiftUp(e) {
    if (e.key == "Shift") {
      this.state.shiftPressed = false;
    }
  }

  handleEnter(e) {
    if (e.key == "Enter" && !this.state.shiftPressed) {
      let time = new Date();
      let to2char = ChatInput.to2char;
      this.props.addMessage({
        userAvatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
        userName: "Ben",
        userId: "3",
        text: e.target.value,
        date: to2char(time.getHours()) + ':' + to2char(time.getMinutes()) + ':' + to2char(time.getSeconds()) + ' '
                 + time.getFullYear() + '.' + to2char(time.getMonth()) + '.' + to2char(time.getDate())
      });
      e.target.value = "";
    }
  }

  render() {
    return(
      <div className="ChatInput">
        <textarea onKeyPress={this.handleEnter} onKeyDown={this.handleShiftDown} onKeyUp={this.handleShiftUp}>

        </textarea>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {
  addMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatInput);
