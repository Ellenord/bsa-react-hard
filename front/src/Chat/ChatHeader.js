import React from 'react';
import './ChatHeader.css';

import { connect } from 'react-redux';
import { getUsersCount } from '../UserList/actions';

class ChatHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      usersCount: 0
    };
  }

  componentDidMount() {
    this.props.getUsersCount();
  }

  render() {
    const data = this.state;
    return (
      <div className="ChatHeader">
        <div className="ChatHeaderContent">
          <div className="ChatHeaderLeft">
            <div className="ChatName">
              <strong>TopSecret</strong>
            </div>
            <div className="ParticipantsCount">
              <strong>{this.props.usersCount + " participants"}</strong>
            </div>
            <div className="MessageCount">
              <strong>{this.props.messages.length} messages</strong>
            </div>
          </div>
          <div className="ChatHeaderRight">
            {this.props.messages.length > 0 &&
              <i>Last message at: {this.props.messages[this.props.messages.length - 1].date}</i>}
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    messages: state.messageListReducer,
    usersCount: state.userListReducer
  };
};

const mapDispatchToProps = {
  getUsersCount
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatHeader);
