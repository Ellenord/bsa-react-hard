import React from 'react';
import './ChatMessage.css';
import like from '../img/like.png';
import dislike from '../img/notlike.svg';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import { connect } from 'react-redux';
import { deleteMessage } from '../MessageList/actions';
import { selectMessage } from '../EditForm/actions';

class ChatMessage extends React.Component {

  constructor(props) {
    super(props);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.selectMessage = this.selectMessage.bind(this);
  }

  deleteMessage(e) {
    this.props.deleteMessage(this.props.id);
  }

  selectMessage(e) {
    this.props.selectMessage(this.props.selfMessage);
  }

  render() {

    return(
      <div className={'FlexWrapper' + (this.props.currentUserId == this.props.userId ? ' SelfMessage' : '')}>
        <div className='ChatMessage'>
          <div className="ChatMessageContent">
            <img src={this.props.userAvatar} />
            <p>
            <span>{this.props.userName}</span><br/>
            {this.props.text}
            </p>
          </div>
          <div className="ChatMessageController">
            <i>{this.props.date}</i>
            <label for={this.props.id}>
              <img className="ChatMessageLike" src={like} />
              <input type="checkbox" id={this.props.id} />
              <img className="ChatMessageDislike" src={dislike} />
            </label>
          </div>
          {(this.props.currentUserId == -1 || this.props.currentUserId == this.props.userId) &&
          <div className="SelfMessageController">
            <button onClick={this.deleteMessage}>Delete</button>
            <button onClick={this.selectMessage}><Link to={"/chat/edit/" + this.props.id}>Edit</Link></button>
          </div>
          }
        </div>
        <div className="FreeSpace"></div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    currentUserId: state.authorizationReducer.id
  };
};

const mapDispatchToProps = {
  deleteMessage, selectMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatMessage);
