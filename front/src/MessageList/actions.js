import {
  INIT_MESSAGES, INIT_MESSAGES_SUCCESS,
  ADD_MESSAGE, ADD_MESSAGE_SUCCESS,
  UPDATE_MESSAGE, UPDATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE, DELETE_MESSAGE_SUCCESS
} from './actionTypes';

export const initMessages = () => ({
  type: INIT_MESSAGES
});
export const initMessagesSuccess = (messages) => ({
  type: INIT_MESSAGES_SUCCESS,
  payload: {
    messages
  }
});

export const addMessage = (data) => ({
  type: ADD_MESSAGE,
  payload: {
    data
  }
});
export const addMessageSuccess = (message) => ({
  type: ADD_MESSAGE_SUCCESS,
  payload: {
    message
  }
});

export const updateMessage = (message) => ({
  type: UPDATE_MESSAGE,
  payload: {
    message
  }
});
export const updateMessageSuccess = (message) => ({
  type: UPDATE_MESSAGE_SUCCESS,
  payload: {
    message
  }
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});
export const deleteMessageSuccess = (id) => ({
  type: DELETE_MESSAGE_SUCCESS,
  payload: {
    id
  }
});
