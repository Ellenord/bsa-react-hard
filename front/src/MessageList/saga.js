import { all, call, put, takeEvery } from 'redux-saga/effects';
import {
  INIT_MESSAGES,
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE
} from './actionTypes';
import {
  initMessagesSuccess,
  addMessageSuccess,
  updateMessageSuccess,
  deleteMessageSuccess
} from './actions';
import makeHttpRequest from '../HttpService';

const targetUrl = 'http://localhost:8080/messages';

function* initMessages(action) {
   try {
      const messages = yield makeHttpRequest(targetUrl, 'GET');
      yield put(initMessagesSuccess(JSON.parse(messages)));
   } catch (e) {}
}

function* addMessage(action) {
  try {
    const { data } = action.payload;
    const message = JSON.parse(yield makeHttpRequest(targetUrl, 'POST', data));
    yield put(addMessageSuccess(message));
  } catch (e) {}
}

function* updateMessage(action) {
  try {
    const { message } = action.payload;
    yield makeHttpRequest(targetUrl, 'PUT', message);
    yield put(updateMessageSuccess(message));
  } catch (e) {}
}

function* deleteMessage(action) {
  try {
    const { id } = action.payload;
    yield makeHttpRequest(targetUrl + '/' + id, 'DELETE');
    yield put(deleteMessageSuccess(id));
  } catch (e) {}
}

function* messageListSaga() {
  yield all([
    takeEvery(INIT_MESSAGES, initMessages),
    takeEvery(ADD_MESSAGE, addMessage),
    takeEvery(UPDATE_MESSAGE, updateMessage),
    takeEvery(DELETE_MESSAGE, deleteMessage),
  ]);
}

export default messageListSaga;
