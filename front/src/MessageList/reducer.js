import {
  INIT_MESSAGES_SUCCESS,
  ADD_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_SUCCESS
} from './actionTypes';

const initialState = [];

export default function(state = initialState, action) {
  switch (action.type) {
    case INIT_MESSAGES_SUCCESS: {
      let { messages } = action.payload;
      return messages;
    }
    case ADD_MESSAGE_SUCCESS: {
      const { message } = action.payload;
      return [...state, message];
    }
    case UPDATE_MESSAGE_SUCCESS: {
      const { message } = action.payload;
      const updated = state.map(i => {
        if (i.id == message.id) {
          return message;
        } else {
          return i;
        }
      });
      return updated;
    }
    case DELETE_MESSAGE_SUCCESS: {
      const { id } = action.payload;
      return state.filter(message => message.id != id);
    }
    default:
      return state;
  }
};
