import { all, fork } from 'redux-saga/effects';
import messageListSaga from './MessageList/saga';
import userListSaga from './UserList/saga';
import authorizationSaga from './Login/saga';
import messageEditFormSaga from './EditForm/saga';

export default function* rootSaga() {
  yield all([
    fork(authorizationSaga),
    fork(messageListSaga),
    fork(userListSaga),
    fork(messageEditFormSaga)
  ]);
}
