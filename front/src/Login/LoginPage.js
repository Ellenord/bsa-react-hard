import React from 'react';
import {
  Redirect
} from 'react-router-dom';
import { connect } from 'react-redux';
import './LoginPage.css';
import { tryLogIn } from './actions';

class LoginPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: ''
    };
    this.onLoginChange = this.onLoginChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onLogInClick = this.onLogInClick.bind(this);
  }

  onLoginChange(e) {
    this.state.login = e.target.value;
  }

  onPasswordChange(e) {
    this.state.password = e.target.value;
  }

  onLogInClick(e) {
    this.props.tryLogIn(this.state.login, this.state.password);
  }

  render() {

    if (this.props.id != null) {
      return <Redirect to="/chat" />
    }

    return(
      <div className="Container">
        <div className="LoginForm">
          <strong>LOGIN</strong>
          <input type="text" onChange={this.onLoginChange} />
          <strong>PASSWORD</strong>
          <input type="password" onChange={this.onPasswordChange} />
          <button onClick={this.onLogInClick}>LOG IN</button>
        </div>
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  return {
    id: state.authorizationReducer.id
  };
};

const mapDispatchToProps = {
  tryLogIn
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
