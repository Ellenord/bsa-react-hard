import {
  TRY_LOG_IN, LOG_IN_SUCCESS, LOG_OUT
} from './actionTypes';

export const tryLogIn = (login, password) => ({
  type: TRY_LOG_IN,
  payload: {
    login, password
  }
});
export const logInSuccess = (id, name, avatar) => ({
  type: LOG_IN_SUCCESS,
  payload: {
    id, name, avatar
  }
});

export const logOut = () => ({
  type: LOG_OUT
});
