import { put, takeEvery } from 'redux-saga/effects';
import { TRY_LOG_IN } from './actionTypes';
import { logInSuccess, logOut } from './actions';
import makeHttpRequest from '../HttpService';

const targetUrl = 'http://localhost:8080/auth';

function* tryLogIn(action) {
  try {
    const response = yield makeHttpRequest(targetUrl, 'POST', action.payload);
    if (response.length != 0) {
      const { id, name, avatar } = JSON.parse(response);
      yield put(logInSuccess(id, name, avatar));
    }
  } catch (e) {}
}

function* authorizationSaga() {
  yield takeEvery(TRY_LOG_IN, tryLogIn);
}

export default authorizationSaga;
