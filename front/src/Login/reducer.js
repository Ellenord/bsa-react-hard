import {
  LOG_IN_SUCCESS, LOG_OUT
} from './actionTypes';

const initialState = {
  id: null,
  name: null,
  avatar: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOG_IN_SUCCESS: {
      let { id, name, avatar } = action.payload;
      return {
        id: id,
        name: name,
        avatar: avatar
      };
    }
    case LOG_OUT: {
      return initialState;
    }
    default:
      return state;
  }
};
