async function makeHttpRequest(url, method, data) {
  let response = await fetch(url, {
    method: method,
    body: JSON.stringify(data)
  });
  return await response.text();
}

export default makeHttpRequest;
