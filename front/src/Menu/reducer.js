import { SELECT_MESSAGE, CANCEL_CHANGES, LOADING, LOADING_SUCCESSFULY } from './actionTypes';

const initialState = {
  message: null,
  isLoading: true
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SELECT_MESSAGE: {
      const { message } = action.payload;
      return {
        message: message,
        isLoading: false
      };
    }
    case LOADING_SUCCESSFULY: {
      return {
        message: state.message,
        isLoading: false
      };
    }
    case LOADING: {
      return {
        message: state.message,
        isLoading: true
      };
    }
    case CANCEL_CHANGES: {
      return initialState;
    }
    default:
      return state;
  }
};
