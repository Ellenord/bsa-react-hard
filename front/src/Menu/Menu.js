import React from 'react';

import './Menu.css';

import { connect } from 'react-redux';
import { loading, saveChanges, cancelChanges } from './actions';
import { useParams } from "react-router-dom";
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route
} from 'react-router-dom';
import store from '../store';
import UserLabel from './UserLabel';
import { initUsers } from '../UserList/actions';

import Spinner from '../Utility/Spinner';

class Menu extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.initUsers();
  }

  render() {

    if (this.props.users.length == 0) {
      return <Spinner />;
    }

    return (
      <div className="MenuBase">
        <div className="MenuContainer">
          {this.props.users.map(user =>
            <UserLabel id={user.id} login={user.login}
              password={user.password} name={user.name}
              mail={user.mail} avatar={user.avatar} />
            )
          }
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    users: state.userListReducer
  };
};

const mapDispatchToProps = {
  initUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
