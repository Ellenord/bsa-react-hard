import React from 'react';
import './UserLabel.css';
import like from '../img/like.png';
import dislike from '../img/notlike.svg';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import { connect } from 'react-redux';
import { deleteUser } from '../UserList/actions';

class UserLabel extends React.Component {

  constructor(props) {
    super(props);
    this.onClickDelete = this.onClickDelete.bind(this);
  }

  onClickDelete(e) {
    this.props.deleteUser(this.props.id);
  }

  render() {

    return (
      <div className="UserLabelBase">
        <div>
          <strong>{"ID: " + this.props.id}</strong>
        </div>
        <div>
          <i>{this.props.name}</i>
        </div>
        <div>
          <button onClick={this.onClickDelete}>Delete</button>
        </div>
        <div>
          <button>Edit</button>
        </div>
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {
  deleteUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserLabel);
