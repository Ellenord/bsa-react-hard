import React from 'react';

import './EditForm.css';

import { connect } from 'react-redux';
import { loading, saveChanges, cancelChanges } from './actions';
import { useParams } from "react-router-dom";
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route
} from 'react-router-dom';
import store from '../store';

import Spinner from '../Utility/Spinner';

class EditForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      inputFieldValue: this.props.message.text,
      message: this.props.message
    }
    this.saveChanges = this.saveChanges.bind(this);
    this.cancelChanges = this.cancelChanges.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.loading(this.props.message);
    console.log(this.props.isLoading);
  }

  onChange(e) {
    this.state.inputFieldValue = e.target.value;
  }

  saveChanges() {
    let newMessage = this.props.message;
    newMessage.text = this.state.inputFieldValue;
    this.props.saveChanges(newMessage);
    this.setState({
      id: null,
      inputFieldValue: null
    });
  }

  cancelChanges() {
    this.props.cancelChanges();
    this.setState({
      id: null,
      inputFieldValue: null
    });
  }

  render() {

    console.log("111111111111111111");

    if (this.props.isLoading) {
        console.log("222222222222222222");
      return <Spinner />;
    }
        console.log("333333333333333333");

    return(
      <div className="EditForm">
        <div className="EditFormFrame">
          <textarea onKeyPress={this.onChange}>
              {this.props.message.text}
          </textarea>
          <div className="EdtiFormController">
            <button onClick={this.saveChanges}>Save</button>
            <button onClick={this.cancelChanges}>Cancel</button>
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    isLoading: state.messageEditFormReducer.isLoading
  };
};

const mapDispatchToProps = {
  loading, saveChanges, cancelChanges
};

const ConnectedEditForm = connect(mapStateToProps, mapDispatchToProps)(EditForm);

function EditPage() {
  const { messageId } = useParams();
  const message = store.getState().messageListReducer.filter(message => {
      return messageId == message.id;
  })[0];
  return <ConnectedEditForm id={messageId} message={message}/>;
}

export default EditPage;
