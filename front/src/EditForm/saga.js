import { all, put, takeEvery } from 'redux-saga/effects';
import { SAVE_CHANGES, LOADING } from './actionTypes';
import { cancelChanges, selectMessage, loadingSuccessfuly } from './actions';
import { updateMessage } from '../MessageList/actions';

import store from '../store';

function* saveChanges(action) {
  try {
    const { message } = action.payload;
    yield put(updateMessage(message));
    yield put(cancelChanges());
  } catch (e) {}
}

function* loading(action) {
  try {
    const { message } = action.payload;
    yield put(selectMessage(message));
    yield put(loadingSuccessfuly());
  } catch (e) {}
}

function* messageEditFormSaga() {
  yield all([
    takeEvery(SAVE_CHANGES, saveChanges),
    takeEvery(LOADING, loading)
  ]);
}

export default messageEditFormSaga;
