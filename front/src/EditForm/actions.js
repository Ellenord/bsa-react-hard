import {
  SELECT_MESSAGE,
  SAVE_CHANGES, CANCEL_CHANGES,
  LOADING, LOADING_SUCCESSFULY
} from './actionTypes';

export const selectMessage = (message) => ({
  type: SELECT_MESSAGE,
  payload: {
    message
  }
});

export const saveChanges = (message) => ({
  type: SAVE_CHANGES,
  payload: {
    message
  }
});

export const cancelChanges = () => ({
  type: CANCEL_CHANGES
});


export const loading = (message) => ({
  type: LOADING,
  payload: {
    message
  }
});

export const loadingSuccessfuly = () => ({
  type: LOADING_SUCCESSFULY
});
