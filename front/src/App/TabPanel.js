import React from 'react';
import './TabPanel.css';
import { connect } from 'react-redux';
import { logOut } from '../Login/actions';
import {
  Link
} from "react-router-dom";

class TabPanel extends React.Component {

  constructor(props) {
    super(props);
    this.onLogOutClick = this.onLogOutClick.bind(this);
  }

  onLogOutClick(e) {
    this.props.logOut();
  }

  render() {
    return(
      <div className="TabPanel">
        <div>
          {this.props.id != null && <button onClick={this.onLogOutClick}>LOGOUT</button>}
          {this.props.id == -1 && <button><Link to="/chat">CHAT</Link></button>}
          {this.props.id == -1 && <button><Link to="/menu">MENU</Link></button>}
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    id: state.authorizationReducer.id
  };
};

const mapDispatchToProps = {
  logOut
};

export default connect(mapStateToProps, mapDispatchToProps)(TabPanel);
