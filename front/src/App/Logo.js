import React from 'react';
import './Logo.css';
import logo from './../img/logo.svg'

class Logo extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className="Logo">
        <img src={logo} alt='logo' />
      </div>
    );
  }

}

export default Logo;
