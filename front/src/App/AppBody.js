import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route
} from 'react-router-dom';
import './AppBody.css';

import { connect } from 'react-redux';
import { dispatch } from 'redux';

import Logo from './Logo';
import Chat from '../Chat/Chat';
import Spinner from '../Utility/Spinner';
import LoginPage from '../Login/LoginPage';
import TabPanel from './TabPanel';

import store from '../store';

import { initMessages } from '../MessageList/actions';
import { initUsers } from '../UserList/actions';

import EditForm from '../EditForm/EditForm';

class AppBody extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <div className="None">
        <Logo />
        <TabPanel />
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  return {
    id: state.authorizationReducer.id
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AppBody);
