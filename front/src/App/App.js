import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route
} from 'react-router-dom';
import './AppBody.css';

import { connect } from 'react-redux';
import { dispatch } from 'redux';

import Logo from './Logo';
import Chat from '../Chat/Chat';
import Spinner from '../Utility/Spinner';
import LoginPage from '../Login/LoginPage';
import TabPanel from './TabPanel';
import AppBody from './AppBody';
import Menu from '../Menu/Menu';

import store from '../store';

import { initMessages } from '../MessageList/actions';
import { initUsers } from '../UserList/actions';

import EditPage from '../EditForm/EditForm';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    if (this.props.id == null) {
      return (
        <div>
          <LoginPage />
          <AppBody />
        </div>
      );
    }

    return(
      <div className="AppBody">
        <Router>
          <Switch>
            { this.props.editMessage != null ? <Route path="/chat/edit/:messageId" component={EditPage} /> : null }
            <Route path="/chat" component={Chat} />
            {this.props.id == -1 && <Route path="/menu" component={Menu}/>}
            <Redirect from="/" to="/chat" />
          </Switch>
          <AppBody />
        </Router>
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  return {
    id: state.authorizationReducer.id,
    editMessage: state.messageEditFormReducer.message
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(App);
